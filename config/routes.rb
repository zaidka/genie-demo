# Need to not fail when uri contains curly braces
# This overrides the DEFAULT_PARSER with the UNRESERVED key, including '{' and '}'
# DEFAULT_PARSER is used everywhere, so its better to override it once
module URI
  remove_const :DEFAULT_PARSER
  unreserved = REGEXP::PATTERN::UNRESERVED
  DEFAULT_PARSER = Parser.new(:UNRESERVED => unreserved + "\{\}\^")
end

Genie::Application.routes.draw do
  #get "home/index"
  match '/' => redirect('/devices')

  match '/login' => 'sessions#new', :via => :get
  match '/login' => 'sessions#create', :via => :post
  match '/logout' => 'sessions#destroy'

  #root :to => 'home#index'
  match 'devices' => 'devices#index'
  match 'devices/:id' => 'devices#show', :via => :get
  match 'devices/:id' => 'devices#update', :via => :post

  match 'faults' => 'faults#index'
  match 'faults/:id/retry' => 'faults#retry', :via => :post
  match 'faults/:id' => 'faults#destroy', :via => :delete

  match 'presets' => 'presets#update', :via => :post
  match 'presets' => 'presets#index'
  match 'presets/new' => 'presets#new', :via => :get
  match 'presets/:id/edit' => 'presets#edit', :via => :get
  match 'presets/:id' => 'presets#destroy', :via => :delete

  match 'objects' => 'objects#update', :via => :post
  match 'objects' => 'objects#index'
  match 'objects/new' => 'objects#new', :via => :get
  match 'objects/:id/edit' => 'objects#edit', :via => :get
  match 'objects/:id' => 'objects#destroy', :via => :delete

  match 'files' => 'files#upload', :via => :post
  match 'files' => 'files#index'
  match 'files/new' => 'files#new', :via => :get
  match 'files/:id' => 'files#destroy', :via => :delete, :constraints => { :id => /[0-9A-Za-z_\-\.\/]+/ }

  match 'api/:path' => 'api#index', :constraints => { :path => /[0-9A-Za-z_\-\.\/]+/ }

end
