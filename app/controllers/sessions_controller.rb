class SessionsController < ApplicationController
  def new
    # TODO redirect if user is already logged in
  end

  def create
    username = params[:username]
    password = params[:password]
    if username == 'iqnetworks' and password == 'eyequeuenetworks'
      cookies.signed[:username] = username
      redirect_to '/'
    else
      redirect_to '/login', :alert => 'Invalid username or password'
      return
    end
  end

  def destroy
    reset_session
    cookies.delete :username
    redirect_to '/'
  end
end
