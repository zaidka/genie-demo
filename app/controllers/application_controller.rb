class ApplicationController < ActionController::Base
  protect_from_forgery

  class NotAuthorized < StandardError; end

  rescue_from NotAuthorized, :with => :not_authorized

  private

  def not_authorized
    if logged_in?
      render 'not_authorized', :status => 403
    else
      flash[:error] = "You must be logged in to access this section"
      redirect_to :controller => :sessions, :action => :new, :redirect => request.fullpath
    end
  end

  helper_method :current_user, :logged_in?

  def current_user
    if request.headers.include? "HTTP_USERNAME"
      request.headers["HTTP_USERNAME"]
    else
      cookies.signed[:username]
    end
  end

  def logged_in?
    current_user != nil
  end

  def api_request
    require 'net/http'
    Net::HTTP.new(Rails.configuration.users[current_user][:api_host], Rails.configuration.users[current_user][:api_port])
  end

end
