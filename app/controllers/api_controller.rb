class APIController < ApplicationController
  before_filter :only => [:index, :show] do |controller|
    raise NotAuthorized unless can? :read, 'api'
  end

  before_filter :only => [:update] do |controller|
    raise NotAuthorized unless can? :write, 'api'
  end

  def index
    require 'net/http'
    http = Net::HTTP.new(Rails.configuration.genied_host, 7557)
    res = http.send_request(request.method, "/#{params[:path]}", nil)
    
    self.response.status = res.code
    res.each_header { |key,value|
      self.response.headers[key] = value
    }
    self.response.headers.delete('transfer-encoding')
    self.response.headers['Content-Type'] = res['Content-Type']

    self.response_body = res.body
  end

end
