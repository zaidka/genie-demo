class FaultsController < ApplicationController
  require 'json'

  def find_faults(query, skip = 0, limit = 0)
    query = {
      'query' => ActiveSupport::JSON.encode(query),
      'skip' => skip,
      'limit' => limit
    }
    http = api_request
    res = http.get("/tasks/?#{query.to_query}")
    @total = res['Total'].to_i
    return ActiveSupport::JSON.decode(res.body)
  end

  # GET /faults
  # GET /faults.json
  def index
    raise NotAuthorized if not logged_in?
    query = {'fault' => {'$exists' => 1}}
    skip = params.include?(:page) ? (Integer(params[:page]) - 1) * 30 : 0
    @faults = find_faults(query, skip, 30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @faults }
    end
  end

  # POST /faults/:id/retry
  def retry
    raise NotAuthorized if not logged_in?
    http = api_request
    res = http.post("/tasks/#{params[:id]}/retry", nil)
    if res.code == '200'
      flash[:success] = 'Task updated'
    else
      flash[:error] = "Unexpected error (#{res.code})"
    end

    redirect_to :action => :index
  end

  # DELETE /faults/1
  # DELETE /faults/1.json
  def destroy
    raise NotAuthorized if not logged_in?
    http = api_request
    res = http.delete("/tasks/#{params[:id]}", nil)
    if res.code == '200'
      flash[:success] = 'Faulty task deleted'
    else
      flash[:error] = "Unexpected error (#{res.code})"
    end

    redirect_to :action => :index
  end
end
