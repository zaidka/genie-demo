class DevicesController < ApplicationController
  require 'net/http'
  require 'json'

  def flatten_params(params, prefix = nil)
    output = []
    for n, v in params
      next if n.start_with?('_')
      if v.include?('_value')
        v['_path'] = "#{prefix}#{n}"
        v['_type'] = v['_value'].class
        output << v
      else
        output += flatten_params(v, prefix ? "#{prefix}#{n}." : "#{n}.")
      end
    end
    return output
  end

  def get_device(id)
    query = {
      'query' => ActiveSupport::JSON.encode({'_id' => URI.escape(id)}),
    }
    http = api_request
    res = http.get("/devices/?#{query.to_query}")
    @now = res['Date'].to_time
    return ActiveSupport::JSON.decode(res.body)[0]
  end

  def find_devices(query, skip = 0, limit = 10, sort = nil)
    query = {
      'query' => ActiveSupport::JSON.encode(query),
      'skip' => skip,
      'limit' => limit,
      'projection' => 'summary',
    }
    query['sort'] = ActiveSupport::JSON.encode(sort) if sort
    http = api_request
    res = http.get("/devices/?#{query.to_query}")
    @total = res['Total'].to_i
    @now = res['Date'].to_time
    return ActiveSupport::JSON.decode(res.body)
  end

  # GET /devices
  # GET /devices.json
  def index
    raise NotAuthorized if not logged_in?
    skip = params.include?(:page) ? (Integer(params[:page]) - 1) * 30 : 0
    if params.include?(:sort)
      sort_param = params[:sort].start_with?('-') ? params[:sort][1..-1] : params[:sort]
      sort_dir = params[:sort].start_with?('-') ? -1 : 1
      sort = {sort_param => sort_dir}
    else
      sort = nil
    end

    @query = {}
    if params.has_key?('query')
      @query = ActiveSupport::JSON.decode(URI.unescape(params['query']))
    end
    if request.format == Mime::CSV
      @devices = find_devices(@query, 0, 0)
    else
      @devices = find_devices(@query, skip, 30, sort)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.csv
      format.json { render json: @devices }
    end
  end

  def find_files(query, skip = 0, limit = 10)
    query = {
      'query' => ActiveSupport::JSON.encode(query),
      'skip' => skip,
      'limit' => limit
    }
    http = api_request
    res = http.get("/files/?#{query.to_query}")
    @total = res['Total'].to_i
    return ActiveSupport::JSON.decode(res.body)
  end

  # GET /devices/1
  # GET /devices/1.json
  def show
    raise NotAuthorized if not logged_in?
    @device = get_device(params[:id])
    @device_params = flatten_params(@device['InternetGatewayDevice'])
    @files = find_files({})
    mac = @device['summary.mac']['_value'].gsub(':', '') rescue nil
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @device }
    end
  end

  def update
    raise NotAuthorized if not logged_in?
    if params.include? 'refresh_summary'
      task = {'name' => 'getParameterValues', 'parameterNames' => ['summary']}
      http = api_request
      res = http.post("/devices/#{URI.escape(params[:id])}/tasks?timeout=3000&connection_request", ActiveSupport::JSON.encode(task))

      if res.code == '200'
        flash[:success] = 'Device refreshed'
      elsif res.code == '202'
        flash[:warning] = 'Device is offline'
      else
        flash[:error] = "Unexpected error (#{res.code})"
      end
    end

    if params.include? 'add_tag'
      tag = ActiveSupport::JSON.decode(params['add_tag']).strip
      http = api_request
      res = http.post("/devices/#{URI.escape(params[:id])}/tags/#{tag}", nil)

      if res.code == '200'
        flash[:success] = 'Tag added'
      else
        flash[:error] = "Unexpected error (#{res.code})"
      end
    end

    if params.include? 'remove_tag'
      tag = ActiveSupport::JSON.decode(params['remove_tag']).strip
      http = api_request
      res = http.delete("/devices/#{URI.escape(params[:id])}/tags/#{tag}", nil)

      if res.code == '200'
        flash[:success] = 'Tag removed'
      else
        flash[:error] = "Unexpected error (#{res.code})"
      end
    end

    if params.include? 'commit'
      tasks = ActiveSupport::JSON.decode(params['commit'])
      perms = []
      for t in tasks
        case t['name']
        when 'reboot'
          perms << [:update, "devices/<#{params[:id]}>/reboot/"]
        when 'factoryReset'
          perms << [:update, "devices/<#{params[:id]}>/factory_reset/"]
        when 'download'
          perms << [:update, "devices/<#{params[:id]}>/download/", t['filename']]
        when 'setParameterValues'
          for v in t['parameterValues']
            perms << [:update, "devices/<#{params[:id]}>/parameters/<#{v[0]}>/", v[1].to_s]
          end
        when 'getParameterValues'
          for n in t['parameterNames']
            perms << [:read, "devices/<#{params[:id]}>/parameters/<#{n}>/"]
          end
        end
      end

      http = api_request
      for t in tasks
        res = http.post("/devices/#{URI.escape(params[:id])}/tasks?timeout=3000&connection_request", ActiveSupport::JSON.encode(t))
        if res.code == '200'
          flash[:success] = 'Tasks committed'
        elsif res.code == '202'
          flash[:warning] = 'Tasks added to queue and will be committed when device is online'
        else
          flash[:error] = "Unexpected error (#{res.code})"
        end
      end
    end

    #redirect_to :action => :show
    redirect_to "/devices/#{URI.escape(params[:id])}"
  end

end
