# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

window.addValueConfiguration = (container, name = '', value = '') ->
  html = """<div configurationType="value">
      Set
      <input type="text" _name="name" value="#{name}" />
      to
      <input type="text" _name="value" value="#{value}" />
      <a href="#" class="action" onclick="fadeOutAndRemove($(this).parent());return false;">&nbsp;x&nbsp;</a>
    </div>"""

  el = $(html)
  $(container).append(el)
  el.hide().fadeIn(300)


window.addAgeConfiguration = (container, name = '', age = '') ->
  html = """<div configurationType="age">
      Refresh
      <input type="text" _name="name" value="#{name}" />
      every
      <input type="text" _name="age" value="#{age}" />
      seconds
      <a href="#" class="action" onclick="fadeOutAndRemove($(this).parent());return false;">&nbsp;x&nbsp;</a>
    </div>"""

  el = $(html)
  $(container).append(el)
  el.hide().fadeIn(300)


window.addAddTagConfiguration = (container, tag = '') ->
  html = """<div configurationType="add_tag">
      Add tag
      <input type="text" _name="tag" value="#{tag}" />
      <a href="#" class="action" onclick="fadeOutAndRemove($(this).parent());return false;">&nbsp;x&nbsp;</a>
    </div>"""

  el = $(html)
  $(container).append(el)
  el.hide().fadeIn(300)


window.addRemoveTagConfiguration = (container, tag = '') ->
  html = """<div configurationType="delete_tag">
      Remove tag
      <input type="text" _name="tag" value="#{tag}" />
      <a href="#" class="action" onclick="fadeOutAndRemove($(this).parent());return false;">&nbsp;x&nbsp;</a>
    </div>"""

  el = $(html)
  $(container).append(el)
  el.hide().fadeIn(300)


window.addAddObjectConfiguration = (container, object = '') ->
  html = """<div configurationType="add_object">
      Add object
      <input type="text" _name="object" value="#{object}" />
      <a href="#" class="action" onclick="fadeOutAndRemove($(this).parent());return false;">&nbsp;x&nbsp;</a>
    </div>"""

  el = $(html)
  $(container).append(el)
  el.hide().fadeIn(300)


window.addRemoveObjectConfiguration = (container, object = '') ->
  html = """<div configurationType="delete_object">
      Remove object
      <input type="text" _name="object" value="#{object}" />
      <a href="#" class="action" onclick="fadeOutAndRemove($(this).parent());return false;">&nbsp;x&nbsp;</a>
    </div>"""

  el = $(html)
  $(container).append(el)
  el.hide().fadeIn(300)


window.initConfigurations = (id) ->
  f = $("##{id}")
  configurations = JSON.parse(f.children('input[name=configurations]').attr('value'))
  container_selector = "##{id} > .configurations_container"
  container = $(container_selector)

  for c in configurations
    switch c['type']
      when 'value'
        addValueConfiguration(container_selector, c['name'], c['value'])
      when 'age'
        addAgeConfiguration(container_selector, c['name'], c['age'])
      when 'add_tag'
        addAddTagConfiguration(container_selector, c['tag'])
      when 'delete_tag'
        addRemoveTagConfiguration(container_selector, c['tag'])
      when 'add_object'
        addAddObjectConfiguration(container_selector, c['object'])
      when 'delete_object'
        addRemoveObjectConfiguration(container_selector, c['object'])

  popup = """
    <a href="#" class="action">&nbsp;+&nbsp;</a>
    <div class="popup">
      <a href="#" class="action" onclick="addValueConfiguration('#{container_selector}');return false;">Set</a><br/>
      <a href="#" class="action" onclick="addAgeConfiguration('#{container_selector}');return false;">Refresh</a><br/>
      <a href="#" class="action" onclick="addAddTagConfiguration('#{container_selector}');return false;">Add tag</a><br/>
      <a href="#" class="action" onclick="addRemoveTagConfiguration('#{container_selector}');return false;">Remove tag</a><br/>
      <a href="#" class="action" onclick="addAddObjectConfiguration('#{container_selector}');return false;">Add object</a><br/>
      <a href="#" class="action" onclick="addRemoveObjectConfiguration('#{container_selector}');return false;">Remove object</a><br/>
    </div>
  """
  f.children('.configurations_selection').html(popup)


window.updateConfigurations = (id) ->
  container_selector = "##{id} > .configurations_container"
  configurations = []
  $(container_selector).children().each( () ->
    type = this.getAttribute('configurationtype')
    switch type
      when 'value'
        name = $(this).children('input[_name="name"]').val().trim()
        value = $(this).children('input[_name="value"]').val().trim()
        configurations.push({type: 'value', name: name, value: value})
      when 'age'
        name = $(this).children('input[_name="name"]').val()
        age = $(this).children('input[_name="age"]').val()
        configurations.push({type: 'age', name: name, age: age})
      when 'add_tag'
        tag = $(this).children('input[_name="tag"]').val()
        configurations.push({type: 'add_tag', tag: tag})
      when 'delete_tag'
        tag = $(this).children('input[_name="tag"]').val()
        configurations.push({type: 'delete_tag', tag: tag})
      when 'add_object'
        object = $(this).children('input[_name="object"]').val()
        configurations.push({type: 'add_object', object: object})
      when 'delete_object'
        object = $(this).children('input[_name="object"]').val()
        configurations.push({type: 'delete_object', object: object})
  )
  $("##{id} > input[name=configurations]").attr('value', JSON.stringify(configurations))
