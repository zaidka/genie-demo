module ApplicationHelper
  def logo_image
    Rails.configuration.users[current_user][:logo] or 'logo.png' rescue 'logo.png'
  end
end
