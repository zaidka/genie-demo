SUMMARY_PARAMS = {
  'summary.serialNumber' => ['InternetGatewayDevice.DeviceInfo.SerialNumber'],
  'summary.manufacturer' => ['InternetGatewayDevice.DeviceInfo.Manufacturer'],
  'summary.ProductClass' => ['InternetGatewayDevice.DeviceInfo.ProductClass'],
  'summary.oui'=> ['InternetGatewayDevice.DeviceInfo.ManufacturerOUI'],
  'summary.hardwareVersion' => ['InternetGatewayDevice.DeviceInfo.HardwareVersion'],
  'summary.softwareVersion' => ['InternetGatewayDevice.DeviceInfo.SoftwareVersion'],
  'summary.mac' => ['InternetGatewayDevice.WANDevice.1.WANConnectionDevice.1.WANIPConnection.1.MACAddress'],

  'summary.bsid' => ['InternetGatewayDevice.WiMAX.Status.BSID', 'InternetGatewayDevice.X_CommunicationsInc_WiMAX_device_metrics.ServingBSID', 'InternetGatewayDevice.WANDevice.1.WiMAXInterfaceConfig.RadioModule.ServBSID'],
  'summary.ip' => ['InternetGatewayDevice.WANDevice.1.WANConnectionDevice.1.WANIPConnection.1.ExternalIPAddress'],
  'summary.cinr' => ['InternetGatewayDevice.WiMAX.Status.CINR1', 'InternetGatewayDevice.X_CommunicationsInc_WiMAX_device_metrics.Statistics.DownlinkMeanCINR', 'InternetGatewayDevice.X_MTK_WiMAX_Param.CINR_level'],
  'summary.rssi' => ['InternetGatewayDevice.WiMAX.Status.RSSI', 'InternetGatewayDevice.X_CommunicationsInc_WiMAX_device_metrics.Statistics.DownlinkMeanRSSI', 'InternetGatewayDevice.X_MTK_WiMAX_Param.RSSI_level'],
  'summary.downlinkModulation' => ['InternetGatewayDevice.WiMAX.Status.DLFEC', 'InternetGatewayDevice.X_CommunicationsInc_WiMAX_device_metrics.Statistics.PHYDLmcs'],
  'summary.uplinkModulation' => ['InternetGatewayDevice.WiMAX.Status.ULFEC', 'InternetGatewayDevice.X_CommunicationsInc_WiMAX_device_metrics.Statistics.PHYULmcs'],
}

def get_value_from_path(device, path)
  ref = device
  nodes = path.split('.')
  for n in nodes
    ref = ref[n]
  end
  return ref
end

def get_device_summary(device)
  summary = {}
  for k, v in SUMMARY_PARAMS
    for p in v
      begin
        summary[k] = get_value_from_path(device, p)
        summary[k]['_path'] = p
        break # break if no error raised
      rescue
      end
    end
  end
  return summary
end

def get_all_summary_paths()
  paths = []
  for k,v in SUMMARY_PARAMS
    for p in v
      paths << p
    end
  end
  return paths
end
